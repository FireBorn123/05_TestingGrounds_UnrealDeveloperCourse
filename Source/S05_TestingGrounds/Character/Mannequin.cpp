// TrevorRandall @TrevorRandall inc

#include "Mannequin.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "../Weapons/Gun.h"


// Sets default values
AMannequin::AMannequin()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a FP Mesh component
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh1P"));
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

}

// Called when the game starts or when spawned
void AMannequin::BeginPlay()
{
	Super::BeginPlay();

	//Spawn Gun Actor into world
	if (GunBlueprint == nullptr) 
	{
		UE_LOG(LogTemp, Warning, TEXT("No Gun BP"))
		return;
	}

	FP_GunInstance = GetWorld()->SpawnActor<AGun>(GunBlueprint);

	if (!ensure(FP_GunInstance)) {UE_LOG(LogTemp, Warning, TEXT("No Gun Instance"))
		return;	}

	if (IsPlayerControlled())
	{
		FP_GunInstance->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
	}
	else {
		FP_GunInstance->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
	}

	if (FP_GunInstance != nullptr)
	{
		FP_GunInstance->FP_AnimInstance = Mesh1P->GetAnimInstance();
		FP_GunInstance->TP_AnimInstance = GetMesh()->GetAnimInstance();
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("FP or TP gun not present"))
	}
	
	
	if (InputComponent != nullptr)
	{
		InputComponent->BindAction("Fire", IE_Pressed, this, &AMannequin::PullTrigger);
	}
}

// Called every frame
void AMannequin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMannequin::UnPossessed()
{
	Super::UnPossessed();
	if( FP_GunInstance != NULL)
	{
		FP_GunInstance->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
	}
}

// Called to bind functionality to input
void AMannequin::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AMannequin::PullTrigger()
{
	FP_GunInstance->OnFire();
}